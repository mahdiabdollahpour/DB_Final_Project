

drop procedure if exists `createCustomer`;
drop procedure if exists `makeOrder`;
drop procedure if exists `giveToPaik`;
drop procedure if exists `addShopSupport`;
drop procedure if exists `addOperationAgent`;
drop procedure if exists `addPaikAgent`;
drop procedure if exists `addPhoneToCustomer`;
drop procedure if exists `addAddressToCustomer`;
drop procedure if exists `addShopSupport`;
drop procedure if exists `addShopSupport`;
drop procedure if exists `createShop`;
drop procedure if exists `createProduct`;
drop procedure if exists `operateOrders`;
drop procedure if exists `deliverOrders`;
drop procedure if exists `orderAfterUpdate`;
drop procedure if exists `addProductToOrder`;
drop procedure if exists `reportReject`;
drop procedure if exists `orderlog`;
drop procedure if exists `addcredit`;


DELIMITER $$
USE `dbfinal`$$


CREATE procedure `createCustomer`(
    IN username varchar(100) ,
    IN pass varchar(255),
    IN email varchar(100),
    IN firstname varchar(100),
    IN lastname varchar(100),
    IN postnumber varchar(255),
    IN sex bit,
    IN credit INT) BEGIN
    
    INSERT into customer (username,pass,email,firstname,lastname,postnumber,sex,credit)
    values
    (username,PASSWORD(pass),email,firstname,lastname,postnumber,sex,credit);
END$$
/*
order status 
 1 = sabt
 2 = ersal
 3 = takmil

*/

create procedure `makeOrder`(
productID INT,SHID INT,cID INT,address varchar(255),paymentType BIT,buyamount INT)begin
	select count(*) into @adds from customeraddress 
    where customeraddress.customerID = cID and customeraddress.address = address;
    #select @adds;
	if(@adds>0) then
		insert into orderTable (shopID ,customerID ,orderStatus ,paymentType ,orderDate ,address)
		values (SHID,cID,'1',paymentType,current_date,address);
		insert into orderProduct(orderID,productID,amount) values ((select max(ID) from orderTable),productID,buyamount);
    end if;
END$$

create procedure `addProductToOrder`(
oid INT,pid INT,buyamount INT)begin
	select shopid into @pshid from product where id =pid;
	select shopid into @oshid from ordertable where id =oid;
    if(@pshid = @oshid)then
		insert into orderProduct(orderID,productID,amount) values (oid,pid,buyamount);
	end if;
END$$
/*
create procedure `operateOrder`(
id INT)begin
	
    set @productID := (select productID from OrderProduct where OrderProduct.orderID = id);
	
    select @price := cost from Product where ID = @productID;
	
    select @customerID :=customerID from orderTable where orderTable.ID = id;
	set @paymentType :=(select paymentType from orderTable where orderTable.ID = id);
	select @buyamount := amount from OrderProduct where OrderProduct.orderID = id;
	select @SHID := shopID from OrderTable where OrderTable.ID = id;
    select @productID ;
    select @shopamount := max(amount) from product where ID=@productID and shopID=@SHID and amount >= @buyamount;
    select @valid := count(ID) from product where ID=@productID and shopID=@SHID and amount >= @buyamount;
    select @emptyPaik := max(ID) from PaikAgent where stat = '0' and shopID=@SHID;
    
    if (not ( @emptyPaik is null) and @valid > 0) 
    then
		
		update PaikAgent set stat ='1' where ID = @emptyPaik;
		update orderTable set orderStatus='2' where ID=id;
        
        insert into OrderPaik(paikID,orderID) values (@emptyPaik,id);    
		
		update product set amount = (@shopamount - @buyamount) where ID=@productID and shopID=@SHID and '1' in (@valid);
        
        if( @paymentType='0') # credit 
        then
			
			update customer set credit = (credit - @buyamount * @price ) where ID = @customerID;
			select 'credit decreased';
        end if;
    end if;
    

END$$
*/
create procedure `operateOrders`()begin
	update orderTable set orderStatus='2' where orderStatus= 1; ## calls the trigger
END$$

create procedure `deliverOrders`()begin
	update orderTable set orderStatus = 3 where orderStatus = 2 ; ## calls the trigger
END$$

/*
create procedure `giveToPaik`(OID INT)
begin
	declare paik INT;
	select id into paik from paikAgent where stat = '0' limit 1;
	insert into orderpaik (paikID,orderID)
    values (paik,OID);
    update paikAgent set stat = '1' where id = paik;
    

END$$
*/

create procedure `addOperationAgent`(
	ID INT ,
    shopID INT,
    customerID INT,
    orderStatus INT,
    paymentType BIT,
    orderDate date,
    address varchar(100)
    )begin 
	Insert INTO OperationAgent (ID,shopID,firstname,lastname,address,phone,stat,credit) 
    values
    (ID,shopID,firstname,lastname,address,phone,stat,credit);

END$$

create procedure `addPaikAgent`(
	shopID INT,
    firstname varchar(100),
    lastname varchar(255),
    address varchar(100),
    phone varchar(20)
    )begin 
	Insert INTO PaikAgent (shopID,firstname,lastname,address,phone,stat,credit,deliveredNum) 
    values
    (shopID,firstname,lastname,address,phone,'0','0','0');

END$$


create procedure `addShopSupport`(ID INT,
    shopID INT,
    firstname varchar(100),
    lastname varchar(255),
    address varchar(100),
    phone varchar(20)
    )begin 
	Insert INTO ShopSupport(ID,shopID,firstname,lastname,address,phone) 
    values
    (ID,shopID,firstname,lastname,address,phone);

END$$


create procedure `createShop`( 
	shopname varchar(100),
    city varchar(255),
    address varchar(100),
    phone varchar(20),
    managername varchar(100),
    postnumber varchar(255),
	startTime time,
    endTime time
    )begin
	Insert INTO Shop(shopname,city,address,phone,managername,postnumber,startTime,endTime) 
    values (shopname,city,address,phone,managername,postnumber,startTime,endTime);
END$$


create procedure `addAddressToCustomer`(address varchar(255),
customerID INT)begin
	INSERT INTO CustomerAddress(address,customerID) values (address,customerID);
END$$


create procedure `addPhoneToCustomer`(phone varchar(20),
customerID INT)begin
	INSERT INTO Customerphone(phone,customerID) values (phone,customerID);
END$$

create procedure `createProduct`(
	title varchar(255),
    shopID INT,
    cost INT,
    off INT,
    amount INT
    )begin
	INSERT INTO product (title,shopID,cost,off,amount) values (title,shopID,cost,off,amount);
END$$

create procedure `reportReject`(
report varchar(255)	
)begin
	insert into supportAgentReport (report,timeReport)values (report,NOW());
END$$
create procedure `orderlog`(
log varchar(255)	
)begin

	insert into orderstatuslog (DESCRIPTION,timeoflog) values (log,NOW());
END$$
create procedure `customerlog`(
log varchar(255)	
)begin

	insert into customerlog (DESCRIPTION,timeoflog) values (log,NOW());
END$$
create procedure `paiklog`(
log varchar(255)	
)begin

	insert into paiklog (DESCRIPTION,timeoflog) values (log,NOW());
END$$

create procedure `addcredit`(cid INT,amount INT)
begin
	update customer set credit = credit + amount where ID = cid;
	insert into banktransaction(cost,ttime,customerID) values (amount,NOW(),cid);
    call customerlog(concat('credit added for customer id:',cast(cid as char(50))));
end$$


/*
create procedure `orderAfterUpdate`(
	ID INT,stat INT
    )begin
	#select 'freePaik trigger running';
	
    
END$$
*/


DELIMITER ;
