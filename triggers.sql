DROP TRIGGER IF EXISTS `dbfinal`.`ordertable_AFTER_UPDATE`;
DROP TRIGGER IF EXISTS `dbfinal`.`ordertable_BEFORE_INSERT`;
DROP TRIGGER IF EXISTS `dbfinal`.`ordertable_BEFORE_DELETE`;
DROP TRIGGER IF EXISTS `dbfinal`.`customeraddress_AFTER_INSERT`;
DROP TRIGGER IF EXISTS `dbfinal`.`customerphone_AFTER_INSERT`;
DROP TRIGGER IF EXISTS `dbfinal`.`customeraddress_AFTER_DELETE`;
DROP TRIGGER IF EXISTS `dbfinal`.`customerphone_AFTER_DELETE`;
DROP TRIGGER IF EXISTS `dbfinal`.`orderProduct_Before_insert`;

DELIMITER $$
USE `dbfinal`$$
CREATE DEFINER = CURRENT_USER TRIGGER `dbfinal`.`ordertable_AFTER_UPDATE` BEFORE UPDATE ON `ordertable` FOR EACH ROW
BEGIN
	#call orderAfterUpdate(New.ID,NEW.orderStatus);
	#Insert into orderstatuslog values ('dhjkhdj',NOW()); 
	/*
    if (NEW.orderStatus = 1 ) then
		if(old.orderstatus !=1)then
			Insert into orderstatuslog values (CONCAT('status changed to sabt shode (1) id:',CAST(New.ID as char(50))),NOW()); 
		end if;
    end if;
    
    if (NEW.orderStatus = -1) then
		if(old.orderstatus !=-1)then
			Insert into orderstatuslog values (CONCAT('status changed to reject shode (-1) id:',CAST(New.ID as char(50))),NOW()); 
				call reportReject(CONCAT('order rejected order id :',CAST(New.ID as char(50))));
		end if;
    end if;
    */
    if (NEW.orderStatus = 3 ) then
		if(OLD.orderStatus = 2) then
			Insert into orderstatuslog values (CONCAT('status changed to ersal shode (3), done ID:',New.ID),NOW()); 
			
			select paikID into @paikID from OrderPaik where orderID = New.ID;
			#select productID into @productID from orderProduct where orderID = ID;
			
			#select cost into @price from Product where Product.ID in (@productID) order by Product.ID;
			
			#select amount into @buyamount from orderProduct where orderID = New.ID order by ProductID;    
			select credit into @nowCredit from PaikAgent where ID = @paikID;
			select sum(op.amount * p.cost)  into @newcredit from orderProduct as op , Product as p 
			where op.productID = p.ID and op.orderID = New.ID;
			
			update PaikAgent set stat = 0 where ID = @paikID;
			update PaikAgent set credit = @nowCredit + (( @newcredit * 5 ) / 100) where ID = @paikID;
			update PaikAgent set deliveredNum = deliveredNum + 1 where ID = @paikID;
            call paiklog(concat('paik deliverd and got the money id:',cast(@paikID as char(50))));
			delete from orderPaik where paikID = @paikID;
		end if;
	end if;
    
    if (NEW.orderStatus = 2) then
		if(OLD.orderStatus = 1) then
		   
			select customerID into @customerID  from orderTable where orderTable.ID = New.ID;
			set @paymentType :=(select paymentType from orderTable where orderTable.ID = New.ID);
			#select amount into  @buyamount from OrderProduct where OrderProduct.orderID = ID;
			select shopID into @SHID from OrderTable where OrderTable.ID = New.ID;
			select sum((op.amount * p.cost * (100 - p.off) )/ 100)  into @expense from orderProduct as op , Product as p 
			where op.productID = p.ID and op.orderID = ID;
			select count(p.ID) into @satisfied from Product as p , OrderProduct as op 
			where op.productID = p.ID and op.orderID = New.ID and p.amount >= op.amount; 
			select count(p.ID) into @allp from Product as p , OrderProduct as op 
			where op.productID = p.ID and op.orderID = New.ID; 
			select credit into @usercredit from customer where ID = new.ID; 
			select max(ID) INTO  @emptyPaik from PaikAgent where PaikAgent.stat < 1 and PaikAgent.shopID=@SHID;
			
			if ((not ( @emptyPaik is null)) and (@allp = @satisfied) and ((@usercredit >=  @expense) or @paymentType='1'))
			then
				call orderlog(CONCAT('status changed to taid shode (2). ID:',CAST(New.ID as char(50))));
				
		
				update PaikAgent set stat ='1' where PaikAgent.ID = @emptyPaik;
                call paiklog(concat('paik assigned for order paikid:',cast(@emptyPaik as char(50))));
				#update orderTable set orderStatus='2' where orderTable.ID=ID;
				
				insert into OrderPaik(paikID,orderID) values (@emptyPaik,New.ID);    
				
				update product as p , OrderProduct as op set p.amount = (p.amount - op.amount) 
				where p.ID=op.productID and op.orderID = new.ID;
				
				if( @paymentType='0') # credit 
				then					
					update customer set credit = (credit - @expense) where customer.ID = @customerID;
				else #bank
					insert into banktransaction(cost,ttime,customerID) values (@expense,NOW(),@customerID);
				end if;

			else # reject it
			
				set NEW.orderStatus = -1;
				call reportReject(CONCAT('order rejected due not enough product or no paik available or low credit id :',CAST(New.ID as char(50))));
				Insert into orderstatuslog values (CONCAT('status changed to reject shode (-1) (not enough product or no paik available or low credit) id:',CAST(New.ID as char(50))),NOW()); 
			
				
			end if;
			
		end if;
    end if;   
    



END$$
#DELIMITER ;
#DELIMITER $$
#USE `dbfinal`$$

CREATE DEFINER = CURRENT_USER TRIGGER `dbfinal`.`ordertable_BEFORE_INSERT` BEFORE INSERT ON `ordertable` FOR EACH ROW
BEGIN
	declare newID int default 0;


	select auto_increment into newID
	from information_schema.tables
	where table_name = 'ordertable'
	and table_schema = database();
	
	#select new.paymentType into @pt from OrderTable where OrderTable.ID = New.ID;
    #select max(ID+1) into @newID from orderTable;
	select startTime into @st from Shop where ID = new.shopID;
	select endTime into @et from Shop where ID = new.shopID;
	select c.username into @cn from customer as c  where c.ID = New.customerID ;
		
	#call orderlog(CONCAT('cn is:',@cn)); 
			
        if(not(current_time between @st and @et))then
			set new.orderStatus = -1;
			
            call orderlog(CONCAT('status changed to reject shode (-1) (out of time) order id:',CAST(newID as char(50)))); 
			call reportReject(CONCAT('order rejected due shop not being open. order id :',CAST(newID as char(50))));
		
		else
			if((not(@cn != "unregistered" )) and ( new.paymentType  = '0'))then
				set new.orderStatus = -1;
				call orderlog(CONCAT('status changed to reject shode (-1) (unregistered user can not use credit) order id: ',CAST(newID as char(50)),' for :',@cn)); 
				call reportReject(CONCAT('order rejected becasue unregistered user can not use credit. order id : ',CAST(newID as char(50)),' for :',@cn));
			else
				Insert into orderstatuslog values (CONCAT('order was inserted id:',CAST(newID as char(50)),' for :',@cn),NOW()); 
			end if;
		end if;
END$$
#DELIMITER ;

#DELIMITER $$
#USE `dbfinal`$$

CREATE DEFINER = CURRENT_USER TRIGGER `dbfinal`.`ordertable_BEFORE_DELETE` BEFORE DELETE ON `ordertable` FOR EACH ROW
BEGIN
		Insert into orderstatuslog values (CONCAT('order was deleted id:',CAST(OLD.ID as char(50))),NOW()); 
    
END$$


CREATE DEFINER = CURRENT_USER TRIGGER `dbfinal`.`customerphone_AFTER_INSERT` After insert ON `customerphone` FOR EACH ROW
BEGIN
		Insert into customerlog values (CONCAT('phone added for customer id:',CAST(new.customerID as char(50))),NOW()); 
    
END$$
CREATE DEFINER = CURRENT_USER TRIGGER `dbfinal`.`customeraddress_AFTER_INSERT` After insert ON `customeraddress` FOR EACH ROW
BEGIN
		Insert into customerlog values (CONCAT('address added for customer id:',CAST(new.customerID as char(50))),NOW()); 
    
END$$
CREATE DEFINER = CURRENT_USER TRIGGER `dbfinal`.`customerphone_AFTER_DELETE` After delete ON `customerphone` FOR EACH ROW
BEGIN
		Insert into customerlog values (CONCAT('phone removed for customer id:',CAST(old.customerID as char(50))),NOW()); 
    
END$$
CREATE DEFINER = CURRENT_USER TRIGGER `dbfinal`.`customeraddress_AFTER_DELETE` After delete ON `customeraddress` FOR EACH ROW
BEGIN
		Insert into customerlog values (CONCAT('address removed for customer id:',CAST(old.customerID as char(50))),NOW()); 
    
END$$


DELIMITER ;
