create database if not exists dbfinal;

use dbfinal;



create table if not exists Customer (
	ID INT NOT NULL AUTO_INCREMENT unique,
    username varchar(100) unique,
    pass varchar(255),
    email varchar(100),
    firstname varchar(100),
    lastname varchar(100),
    postnumber varchar(255),
    sex bit,
    credit INT,
    primary key(ID)
); 

create table if not exists CustomerAddress (
	customerID INT,
    address varchar(255)
); 

create table if not exists CustomerPhone (
	customerID INT,
    phone varchar(20)
); 
create table if not exists Shop (
	ID INT NOT NULL AUTO_INCREMENT,
    shopname varchar(100) unique,
    city varchar(255),
    address varchar(100),
    phone varchar(20),
    managername varchar(100),
    postnumber varchar(255),
    startTime time,
    endTime time,
	primary key(ID)
    
);
create table if not exists ShopSupport (
	ID INT NOT NULL AUTO_INCREMENT,
    shopID INT,
    firstname varchar(100),
    lastname varchar(255),
    address varchar(100),
    phone varchar(20),
	primary key(ID)
);
create table if not exists OperationAgent (
	ID INT NOT NULL AUTO_INCREMENT,
    shopID INT,
    firstname varchar(100),
    lastname varchar(255),
    address varchar(100),
    phone varchar(20),
	primary key(ID)
);
create table if not exists PaikAgent (
	ID INT NOT NULL AUTO_INCREMENT,
    shopID INT,
    firstname varchar(100),
    lastname varchar(255),
    address varchar(100),
    phone varchar(20),
    stat INT,
    credit INT,
    deliveredNum INT,
	primary key(ID)
);
create table if not exists OrderPaik(
	paikID INT NOT NULL,
    orderID INT NOT NULL
);

create table if not exists OrderTable (
	ID INT NOT NULL AUTO_INCREMENT,
    shopID INT,
    customerID INT,
    orderStatus INT,
    paymentType BIT, /* zero for credit one for dargahe bank  */
    orderDate date,
    address varchar(255),
    #amount INT,
	primary key(ID)

);
create table if not exists Product (
	ID INT NOT NULL AUTO_INCREMENT,
    title varchar(255) not null,
    shopID INT not null,
    cost INT not null,
    off INT not null,
    amount INT not null,
    UNIQUE KEY title_shopID (title,shopID),
	primary key(ID)
);

create table if not exists OrderProduct(
	orderID INT NOT NULL,
    productID INT NOT NULL,
    amount INT NOT NULL
);
create table if not exists OrderStatusLog(
	description VARCHAR(255) NOT NULL,
    timeoflog timestamp NOT NULL
);
create table if not exists CustomerLog(
	description VARCHAR(255) NOT NULL,
    timeoflog timestamp NOT NULL
);
create table if not exists PaikLog(
	description VARCHAR(255) NOT NULL,
    timeoflog timestamp NOT NULL
);

create table if not exists supportAgentReport(
	ID INT NOT NULL AUTO_INCREMENT,
	report VARCHAR(255) NOT NULL,
    timeReport timestamp NOT NULL,
    primary key(ID)
);
create table if not exists bankTransaction(
	ID INT NOT NULL AUTO_INCREMENT,
	cost INT NOT NULL,
    ttime timestamp NOT NULL,
    customerID INT NOT NULL,
    primary key(ID)
);

/*
create table if not exists OrderProduct(
	orderID INT,
    productID INT,
    amount INT

);
create table if not exists ShopProduct(
	shopID INT,
    productID INT,
    amount INT

);

*/
