create database if not exists dbfinal;

use dbfinal;
# username pass email firstname lastname postnumber sex credit INT
call createCustomer("unregistered","unregistered","","unregistered","unregistered","unregistered",b'1','0'); 

call createCustomer("mahdi","1377","mahdi@hj.com","mahdi","abdollah","987",b'1','0'); 
call addAddressTocustomer("addr1",'1');
call addAddressTocustomer("some address",'2');
call addPhoneToCustomer("09125678759",'2');
call addPhoneToCustomer("09375545749",'2');
call addcredit('2',9999999999);

call createShop("some name","tehran","engelab street","95895","some guy","3847984",'000000','235959');
call createShop("second shop","mashhad","23 steet","958954545","ownerzade","3847984",'000000','235959');
call addPaikAgent('1',"jaffar","jaffari","azadi street","475-685787");
call addPaikAgent('1',"paik","paikzade","khiaban paik pelak p","555-3969");

#title shopID cost off amount 
call createProduct("fork",'1','100','10','100');
call createProduct("fork2",'1','150','15','250');
#call createProduct("spoon",'2','120','12','300');

#productID shopID customerID address paymentType amount
call makeOrder('1' ,'1','1',"addr1" ,b'1','20');
call makeOrder('1' ,'1','2',"some address" ,b'0','40');
call makeOrder('1' ,'1','2',"some address" ,b'0','9999999999999');

#oid INT,pid INT,buyamount INT
call addProductToOrder('2','2','50');
call addProductToOrder('2','2','50');

#call addProductToOrder('2','3','30');

call operateOrders();

call deliverorders();

